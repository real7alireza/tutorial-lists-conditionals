import React , {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import Person from './Person/Person'


class App extends Component {
  
  state = {
    persons : [
      { id: 'dfgd1', name : 'Max', age : 30},
      { id: 'drhd1', name : 'Alireza', age : 22},
      { id: 'drg55', name : 'Hossein', age : 15},
    ],
    otherState: 'some other state',
    showPerson : false
  }

  switchNameHandler = (newName) => {
    this.setState({
      persons : [
        {name : newName, age : 30},
        {name : 'Alireza', age : 22},
        {name : 'Hossein', age : 18}  
      ]
    })
  }

  nameChangedHandler = (event , id) => {
    
    const personIndex = this.state.persons.findIndex(p => p.id === id);
    const person = {
      ...this.state.persons[personIndex]
    };
    // const person = Object.assign({}, this.state.persons[personIndex])
    person.name = event.target.value;
    const persons = [...this.state.persons];
    persons[personIndex] = person;
    this.setState({persons : persons});
  }

  togglePerson = () => {
    const show = this.state.showPerson
    this.setState({showPerson: !show})
  }

  deletePersonCard = (personIndex) =>{
    const persons = [...this.state.persons]
    persons.splice(personIndex,1)
    this.setState({persons: persons})
  }
  render(){
    const style = {
      backgroundColor : 'white',
      font : 'inherit',
      border : '1px solid blue',
      padding : '8px',
      cursor : 'pointer'
    }

    let persons = null
    if(this.state.showPerson){
      persons = (
        <div>
          {this.state.persons.map((person ,index) => {
            return <Person
              click={() => this.deletePersonCard(index)} 
              name={person.name}
              age={person.age}
              key={person.id}
              changed={ event =>this.nameChangedHandler(event, person.id)}/>
          })}  
        </div>
      )
    }

    return(
      <div className="App">
        <h1>Hi, I'm React App</h1>
        <p>This is really working</p>
        <button 
          style = {style}
          onClick={this.togglePerson}>Toggle Persons Cards
        </button>
        {persons}       
      </div>
    )
  }
}

export default App;
